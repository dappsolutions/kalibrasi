<?php

class Dashboard extends MX_Controller {

 public $hak_akses;
 public $upt;

 public function __construct() {
  parent::__construct();
  date_default_timezone_set("Asia/Jakarta");
  $this->hak_akses = $this->session->userdata('hak_akses');
  $this->upt = $this->session->userdata('upt_id');
 }

 public function getModuleName() {
  return 'dashboard';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/jquery_min_latest.js"></script>',
      '<script src="' . base_url() . 'assets/js/moment_min.js"></script>',
      '<script src="' . base_url() . 'assets/js/daterangepicker.js"></script>',
      '<link href="' . base_url() . 'assets/css/daterangepicker.css" type="text/css" rel="stylesheet"/>',
      '<script src="' . base_url() . 'assets/js/controllers/dashboard.js"></script>',
  );

  return $data;
 }

 public function index() {
  $data['view_file'] = 'v_index';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Dashboard";
  $data['title_content'] = 'Dashboard';
  $data['kalibrasi'] = $this->getDataKalibrasiJadwal();
  echo Modules::run('template', $data);
 }

 public function getDataKalibrasiJadwal() {
  $date = date('Y-m-d');
  $data = Modules::run('database/get', array(
              'table' => 'kalibrasi k',
              'field' => array('k.*', 'a.nama_alat', 'a.kode_alat'),
              'join' => array(
                  array('alat a', 'k.alat = a.id'),
              ),
              'where' => "k.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $remember_date = str_replace('-', '', $value['remember_date']);
    $remember_date = intval($remember_date);
    $date_now = str_replace('-', '', date('Y-m-d'));
    $date_now = intval($date_now);
    if($date_now >= $remember_date){
     $value['status'] = 'Expired';
     array_push($result, $value);
    }
   }
  }

  return $result;
 }

}
