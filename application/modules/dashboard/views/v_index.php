<input type='hidden' name='' id='hak_akses' class='form-control' value='<?php echo $this->session->userdata('hak_akses') ?>'/>
<link href="<?php echo base_url() ?>assets/plugins/components/vegas/vegas.min.css" rel="stylesheet">
<script src="<?php echo base_url() ?>assets/plugins/components/vegas/vegas.min.js"></script>
<div class="container-fluid">
 <div class='row'>
  <div class='col-md-12'>
   <div class="white-box">
    <div class="row">
     <div class="col-md-12">
      <h4> Daftar Alat yang Perlu Dikalibrasi</h4>
     </div>
    </div>
    <div class="row">
     <div class="col-md-12">
      <div class="table-responsive">
       <table class="table color-bordered-table primary-bordered-table">
        <thead>
         <tr class="">
          <th class="font-12">No</th>
          <th class="font-12">Equipment</th>
          <th class="font-12">Identity No</th>
          <th class="font-12">Validity</th>
          <th class="font-12">Issue Date</th>
          <th class="font-12">Due Date</th>
          <th class="font-12">Conducted By</th>
          <th class="font-12">Status</th>
          <th class="font-12">Manufacture</th>
         </tr>
        </thead>
        <tbody>
         <?php if (!empty($kalibrasi)) { ?>
          <?php $no = 1; ?>
          <?php foreach ($kalibrasi as $value) { ?>
           <tr>
            <td class='font-12'><?php echo $no++ ?></td>
            <td class='font-12'><?php echo $value['nama_alat'] ?></td>
            <td class='font-12'><?php echo $value['kode_alat'] ?></td>
            <td class='font-12'><?php echo $value['validity'] ?></td>
            <td class='font-12'><?php echo $value['issue_date'] ?></td>
            <td class='font-12'><?php echo $value['due_date'] ?></td>
            <td class='font-12'><?php echo $value['conducted_by'] ?></td>
            <?php $label_bg = $value['status'] == 'Certified' ? 'label-success' : 'label-danger' ?>
            <td class='font-12'><label class="label <?php echo $label_bg ?> "><?php echo $value['status'] ?></label></td>
            <td class='font-12'><?php echo $value['manufacture'] ?></td>
           </tr>
          <?php } ?>
         <?php } else { ?>
          <tr>
           <td class="text-center font-12" colspan="10">Tidak Ada Data Ditemukan</td>
          </tr>
         <?php } ?>         
        </tbody>
       </table>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div> 
</div>

<script>
 $("#header_dashboard").vegas({
  slides: [
   {src: '<?php echo base_url() . 'assets/images/background/profile-bg.jpg' ?>'}
  ],
 });
</script>