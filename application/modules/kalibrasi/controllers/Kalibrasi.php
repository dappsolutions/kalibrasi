<?php

class Kalibrasi extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'kalibrasi';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/plugins/components/custom-select/custom-select.min.js" type="text/javascript"></script>',
      '<script src = "' . base_url() . 'assets/plugins/components/bootstrap-select/bootstrap-select.min.js" type = "text/javascript"></script>',
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/kalibrasi.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'kalibrasi';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;


  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Kalibrasi";
  $data['title_content'] = 'Data Kalibrasi';
  $content = $this->getDataKalibrasi();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataKalibrasi($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('a.nama_alat', $keyword),
       array('a.kode_alat', $keyword),
       array('k.validity', $keyword),
       array('k.issue_date', $keyword),
       array('k.due_date', $keyword),
       array('k.conducted_by', $keyword),
       array('k.manufacture', $keyword),
       array('k.status', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' k',
              'field' => array('k.*', 'a.nama_alat', 'a.kode_alat'),
              'join' => array(
                  array('alat a', 'k.alat = a.id'),
              ),
              'where' => "k.deleted = 0"
  ));

  return $total;
 }

 public function getDataKalibrasi($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('a.nama_alat', $keyword),
       array('a.kode_alat', $keyword),
       array('k.validity', $keyword),
       array('k.issue_date', $keyword),
       array('k.due_date', $keyword),
       array('k.conducted_by', $keyword),
       array('k.manufacture', $keyword),
       array('k.status', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' k',
              'field' => array('k.*', 'a.nama_alat', 'a.kode_alat'),
              'join' => array(
                  array('alat a', 'k.alat = a.id'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => "k.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $val_due_date = intval(str_replace('-', '', $value['due_date']));
    $date_now = intval(date('Ymd'));    
    if($val_due_date <= $date_now){
     $value['status'] = 'Expired';
    }else{
     $value['status'] = 'Certified';
    }
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataKalibrasi($keyword)
  );
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Kalibrasi";
  $data['title_content'] = 'Tambah Kalibrasi';
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataKalibrasi($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Kalibrasi";
  $data['title_content'] = 'Ubah Kalibrasi';
  echo Modules::run('template', $data);
 }

 public function getDetailDataKalibrasi($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' k',
              'field' => array('k.*', 'a.nama_alat', 'a.kode_alat'),
              'join' => array(
                  array('alat a', 'a.id = k.alat')
              ),
              'where' => "k.id = '".$id."'"
  ));
  
  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
  }


  return $result;
 }

 public function detail($id) {
  $data = $this->getDetailDataKalibrasi($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Kalibrasi";
  $data['title_content'] = "Detail Kalibrasi";
  echo Modules::run('template', $data);
 }

 public function getPostAlatName($value) {
  $data['nama_alat'] = $value->equipment;
  $data['kode_alat'] = $value->identity;
  return $data;
 }

 public function getPostKalibrasi($value) {
  $data['validity'] = $value->validity;
  $data['issue_date'] = $value->issue_date;
  $data['due_date'] = $value->due_date;
  $data['remember_date'] = $value->remember_date;
  $data['conducted_by'] = $value->conducted_by;
  $data['status'] = $value->status;
  $data['manufacture'] = $value->manufacture;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));

  $id = $this->input->post('id');
  $is_valid = false;
  $message = "";
  $this->db->trans_begin();
  try {
   $post_alat = $this->getPostAlatName($data);
   if ($id == '') {
    //alat
    $alat = Modules::run('database/_insert', "alat", $post_alat);

    //kalibrasi
    $post_kalibrasi = $this->getPostKalibrasi($data);
    $post_kalibrasi['alat'] = $alat;
    $id = Modules::run('database/_insert', $this->getTableName(), $post_kalibrasi);
   } else {
    Modules::run('database/_update', "alat", $post_alat, array('id' => $data->alat));

    //kalibrasi
    $post_kalibrasi = $this->getPostKalibrasi($data);
    $post_kalibrasi['alat'] = $data->alat;
    Modules::run('database/_update', $this->getTableName(), $post_kalibrasi, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id, 'message' => $message));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Kalibrasi";
  $data['title_content'] = 'Data Kalibrasi';
  $content = $this->getDataKalibrasi($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

}
