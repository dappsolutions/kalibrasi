<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<input type='hidden' name='' id='alat' class='form-control' value='<?php echo isset($alat) ? $alat : '' ?>'/>
<div class="container-fluid"> 
 <!--.row-->
 <div class="row">
  <div class="col-md-12">
   <div class="panel panel-primary">
    <div class="panel-heading"> <?php echo $title_content ?></div>
    <div class="panel-wrapper collapse in" aria-expanded="true">
     <div class="panel-body">
      <form action="#" class="form-horizontal">
       <div class="form-body">
        <h3 class="box-title">Kalibrasi <i class="fa fa-arrow-down"></i></h3>
        <hr class="m-t-0 m-b-40">
        <div class="row">
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Equipment</label>
           <div class="col-md-9">
            <input type="text" id='equipment' maxlength="30" class="form-control required" error='Equipment' placeholder="Equipment" value="<?php echo isset($nama_alat) ? $nama_alat : '' ?>">
           </div>
          </div>
         </div>
         <!--/span-->
         <div class="col-md-6">
          <div class="form-group">
           <label class="control-label col-md-3">Identity No</label>
           <div class="col-md-9">
            <input type="text" id='identity' class="form-control required" placeholder="Identity No" error='Identity No' value="<?php echo isset($kode_alat) ? $kode_alat : '' ?>">
           </div>
          </div>
         </div>
         <!--/span-->
        </div>       
       </div>

       <div class="row">
        <div class="col-md-6">
         <div class="form-group">
          <label class="control-label col-md-3">Validity</label>
          <div class="col-md-9">
           <input type="text" id='validity' class="form-control required" placeholder="Validity" error='Validity' value="<?php echo isset($validity) ? $validity : '' ?>">
          </div>
         </div>
        </div>

        <div class="col-md-6">
         <div class="form-group">
          <label class="control-label col-md-3">Issue Date</label>
          <div class="col-md-9">
           <input type="text" id='issue_date' class="form-control required" placeholder="Issue Date" error='Issue Date' value="<?php echo isset($issue_date) ? $issue_date : '' ?>">
          </div>
         </div>
        </div>
       </div>
       
       <div class="row">
        <div class="col-md-6">
         <div class="form-group">
          <label class="control-label col-md-3">Due Date</label>
          <div class="col-md-9">
           <input type="text" id='due_date' class="form-control required" placeholder="Due Date" error='Due Date' value="<?php echo isset($due_date) ? $due_date : '' ?>">
          </div>
         </div>
        </div>

        <div class="col-md-6">
         <div class="form-group">
          <label class="control-label col-md-3">Conducted By</label>
          <div class="col-md-9">
           <input type="text" id='conducted_by' class="form-control required" placeholder="Conducted By" error='Conducted By' value="<?php echo isset($conducted_by) ? $conducted_by : '' ?>">
          </div>
         </div>
        </div>
       </div>
       
       <div class="row">
<!--        <div class="col-md-6">
         <div class="form-group">
          <label class="control-label col-md-3">Status</label>
          <div class="col-md-9">
           <input readonly="" type="text" id='status' class="form-control required" placeholder="Status" error='Status' value="<?php echo isset($status) ? $status : '' ?>">
          </div>
         </div>
        </div>-->

        <div class="col-md-6">
         <div class="form-group">
          <label class="control-label col-md-3">Manufacture</label>
          <div class="col-md-9">
           <input type="text" id='manufacture' class="form-control required" placeholder="Manufacture" error='Manufacture' value="<?php echo isset($manufacture) ? $manufacture : '' ?>">
          </div>
         </div>
        </div>
       </div>
       
       <div class="row">
        <div class="col-md-6">
         <div class="form-group">
          <label class="control-label col-md-3">Remember Date</label>
          <div class="col-md-9">
           <input type="text" id='remember_date' class="form-control required" placeholder="Remember Date" error='Remember Date' value="<?php echo isset($remember_date) ? $remember_date : '' ?>">
          </div>
         </div>
        </div>
       </div>
     </div>

     <div class="form-actions">
      <div class="row">
       <div class="col-md-12">
        <div class="row">
         <div class="col-md-offset-3 col-md-9 text-right">
          <button type="submit" class="btn btn-success" onclick="Kalibrasi.simpan('<?php echo isset($id) ? $id : '' ?>', event)">Submit</button>
          <button type="button" class="btn btn-default" onclick="Kalibrasi.back()">Batal</button>
         </div>
        </div>
       </div>
       <div class="col-md-6"> </div>
      </div>
     </div>
     <br/>
     </form>
    </div>
   </div>
  </div>
 </div>
</div>
<!--./row--> 
</div>
