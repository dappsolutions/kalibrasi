<?php

if (!defined('BASEPATH'))
 exit('No direct script access allowed');

class No_generator extends MX_Controller {

 public function __construct() {
  parent::__construct();
 }

 public function digit_count($length, $value) {
  while (strlen($value) < $length)
   $value = '0' . $value;
  return $value;
 }

 public function generateNoProbis() {
  $no = 'PROBIS' . date('y') . strtoupper(date('M'));
  $data = Modules::run('database/get', array(
  'table' => 'probis',
  'like' => array(
  array('no_probis', $no)
  ),
		'orderby'=> 'id desc'
  ));

  $seq = 1;
  if (!empty($data)) {
   $data = $data->row_array();
   $seq = str_replace($no, '', $data['no_probis']);   
			$seq = intval($seq) + 1;
  }
		
  $seq = $this->digit_count(3, $seq);
  $no .= $seq;
  return $no;
 }
 
 public function generateNoUsulan() {
  $no = 'USULAN' . date('y') . strtoupper(date('M'));
  $data = Modules::run('database/get', array(
  'table' => 'pengajuan_probis',
  'like' => array(
  array('no_probis', $no)
  ),
		'orderby'=> 'id desc'
  ));

  $seq = 1;
  if (!empty($data)) {
   $data = $data->row_array();
   $seq = str_replace($no, '', $data['no_probis']);   
			$seq = intval($seq) + 1;
  }
		
  $seq = $this->digit_count(3, $seq);
  $no .= $seq;
  return $no;
 }

}
